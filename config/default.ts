export default {
    port:8000,
    dburi:"mongodb://localhost:27017/DevSoft",
    saltWorkFactor:10,
    publicKey:`-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJunmq/HFDOLV/UIfSj5urRFMj
Xv9yZ9l7UG8s9CHD9DAcsFbyHhbCrauMAGtGvzlFVv4i6SlhpFbqGfZQi1zxdSVu
40qZnngA+VbLnEtsJUS/OnFX80pFLRm2KieoiOczOY6FoJiBletC2da0WThjlPbT
yLg5ypMK5KaoyeSTlwIDAQAB
-----END PUBLIC KEY-----`,
    privateKey: `-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDJunmq/HFDOLV/UIfSj5urRFMjXv9yZ9l7UG8s9CHD9DAcsFby
HhbCrauMAGtGvzlFVv4i6SlhpFbqGfZQi1zxdSVu40qZnngA+VbLnEtsJUS/OnFX
80pFLRm2KieoiOczOY6FoJiBletC2da0WThjlPbTyLg5ypMK5KaoyeSTlwIDAQAB
AoGBAJQ06XCi+T+u6UF5U+Bx26evkfk5sSlHfPPpO1Tmo+wIelSJhcdqLcSnvZIl
OKuIkDYOXLYfYWS4TZa+rkWsrgH0bQfB9kXQrbMQK9rSxi6Lxr5bphrfGsFP3FpH
e95MqiA0QtGfmgYK6Xd0apv8qhiVSdYwfu+U/nuUplGyTDqpAkEA+bVPRyb0hrhO
+fvn9AfC8o5PvDj8Xxy2TheoqDBUAY90iykiIdhJnuzbooN4TQmWW1bKY4csQ2OO
Z8v5bud8uwJBAM7Pro7nzh98xLf+NNNyky5roIR3B51Fj7U5j0l6tytCIfR7fGsN
ywSc3owyhJl+OR9upWkZS/2Ujl9xK7EGpNUCQQCzM2LJdTNW2z4SOK2le0x0NZ0T
PvIeb8SFFoV67fPbis/gboyILWfoHaMhZagX4SF+OzLTalZhpji0VLWU2ZuxAkEA
wIw58FE/5q8KlGe4FTnycGDCRTJxQ2sVXlwl3PrE+SCMboxeDWn9H+FRjmQmcqZD
VtF0bcKgroqWd6PtUtdltQJASv9h79azvdAJgCG2UH5m5wU5JY4EojfYPZ40Y5sx
KK1BNo3YewrA+UzatR1n8YKnmSn5JglXht+entUmlPXJTw==
-----END RSA PRIVATE KEY-----`,
    accessTokenTtl: "24h",
    refreshTokenTtl: "1y",
}