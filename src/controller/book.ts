import { Request, Response } from "express";
import {
  createBook,
  findAllBooks,
  findBookById,
  updateBookById,
  findauthorById
} from "../services/index";
import * as _ from 'lodash';


export const createNewBook = async (req: Request, res: Response) => {
  const obj = { ...req.body };
  try {
    const result = await createBook(obj);
    res.status(200).send({ isAdded: true, result });
  } catch (error) {
    res.status(401).send({ isAdded: false , message: error });
  }
};

export const getAllBooks = async (req: Request, res: Response) => {
  try {
    const result = await findAllBooks();
    return res.status(200).send({result});
  } catch(error){
    res.status(401).send({ message: error });
  }

};
export const getBookById = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await findBookById(id);
    if(result) {
      const { author } = result;
      const authorDetails = await findauthorById(author);
      const newResult = _.pick(result,['name','isbn','_id']);
      newResult.authorDetails = authorDetails;
      return res.status(200).send({newResult});

    }
  } catch(error){
    res.status(401).send({ message: error });
  }

};
export const editBookById = async (req: Request, res: Response) => {
  const  obj  = req.body;
  const _id = req.params;
  const newObj = _.omit(obj,['_id','index']);
  try {
    const result = await updateBookById(_id,newObj);
    if(result) {
      return res.status(200).send({ success: true });
    }
  } catch(error){
    res.status(401).send({ message: error, success:false });
  }

};
