import { Request, Response } from "express";
import {
  createauthor,
  findAllauthors,
  findauthorById,
  updateauthorById
} from "../services/index";
import * as _ from 'lodash';

export const createNewauthor = async (req: Request, res: Response) => {
  const obj = { ...req.body };
  try {
    const result = await createauthor(obj);
    res.status(200).send({ isAdded: true, result });
  } catch (error) {
    res.status(401).send({ isAdded: false , message: error });
  }
};

export const getAllauthors = async (req: Request, res: Response) => {
  try {
    const result = await findAllauthors();
    return res.status(200).send({result});
  } catch(error){
    res.status(401).send({ message: error });
  }

};
export const getauthorById = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await findauthorById(id);
    return res.status(200).send(result);
  } catch(error){
    res.status(401).send({ message: error });
  }

};
export const editauthorById = async (req: Request, res: Response) => {
  const  obj  = req.body;
  const _id  = req.params;
  const newObj = _.omit(obj,['_id','index']);
  try {
    const result = await updateauthorById(_id,newObj);
    if(result){
      return res.status(200).send({ success:true });
    }

  } catch(error){
    res.status(401).send({ message: error });
  }

};
