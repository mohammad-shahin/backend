import{ BookDocument, BookModel } from '../models/index';
import * as _ from 'lodash';
import mongoose,{ FilterQuery, UpdateQuery } from 'mongoose';

export async function createBook(bookObject: Object) {
    const book = await BookModel.create(bookObject);
    return book.toJSON();
  }

export async function findAllBooks(){
    return await BookModel.find({});
}
export async function findBookById(_id:mongoose.Types.ObjectId){
    return await BookModel.findOne({_id});
}
export async function updateBookById(query:FilterQuery<BookDocument>,update:UpdateQuery<BookDocument>){
    return BookModel.updateOne(query,update)
}
