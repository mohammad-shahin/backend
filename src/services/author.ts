import { AuthorDocument, authorModel } from "../models/index";
import mongoose,{FilterQuery,UpdateQuery} from 'mongoose';
import * as _ from "lodash";

export async function createauthor(authorObject: Object) {
  const author = await authorModel.create(authorObject);
  return author.toJSON();
}
export async function findAllauthors() {
  return await authorModel.find({});
}
export async function findauthorById(_id: mongoose.Types.ObjectId) {
  return await authorModel.findOne({ _id });
}
export async function updateauthorById(query: FilterQuery<AuthorDocument>, update: UpdateQuery<AuthorDocument> ) {
  return authorModel.updateOne(query, update);
}
