import express from 'express';
import config from 'config';
import dbConnect from './utiles/connect';
import log from './utiles/logger';
import routes from './routes';
import {deserialize} from './middleware/deserializeUser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
const port = config.get<number>('port');
const app = express();
const http = require('http').Server(app)
app.use(express.json());
app.use(cors({ origin: ['http://localhost:3000','http://localhost:3001'], credentials: true }));
dbConnect.getInstance()
app.use(cookieParser());
//app.use(deserialize);

http.listen(port,async ()=>{
    log.info(`APP is running on port ${port}`);
    routes(app)
})