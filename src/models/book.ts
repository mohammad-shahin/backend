import mongoose from "mongoose";
import { AuthorDocument } from "./author";
export interface BookDocument extends mongoose.Document {
    author: AuthorDocument["_id"];
    name: string;
    isbn: string;
}
const BookSchema = new mongoose.Schema({
    author:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"athor"
    },
    name:{
        type:String,
        required:true,
    },
    isbn:{
        type:String,
        required:String

    }
}, { timestamps: true})

export const BookModel = mongoose.model<BookDocument>("books", BookSchema);
