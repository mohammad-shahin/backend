import mongoose from "mongoose";

export interface AuthorDocument extends mongoose.Document {
  firtName: string;
  lastName: string;
}

const authorSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

export const authorModel = mongoose.model<AuthorDocument>("author", authorSchema);
