import { Express } from "express";
import {
  createNewBook,
  getAllBooks,
  getBookById,
  editBookById,
  createNewauthor,
  getAllauthors,
  getauthorById,
  editauthorById
} from "./controller/index";

function routes(app: Express) {

  app.post("/api/book", createNewBook);
  app.get("/api/book", getAllBooks);
  app.get("/api/book/:id", getBookById);
  app.put("/api/book/:id", editBookById);
  app.post("/api/author", createNewauthor);
  app.get("/api/author", getAllauthors);
  app.get("/api/author/:id", getauthorById);
  app.put("/api/author/:id", editauthorById);
}

export default routes;
