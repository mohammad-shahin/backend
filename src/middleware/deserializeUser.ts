// import { Request, Response, NextFunction } from "express";
// import { verifyJWT } from "../utiles/jwt";
// import * as _ from "lodash";
// import { reIssueAccessToken } from "../services/book";
// export const deserialize = async (req: Request, res: Response, next: NextFunction) => {
//   const accessToken = req.cookies.token;
//   const refreshToken = _.get(req, "headers.x-refresh");
//   const { decoded, expired } = verifyJWT(accessToken);
//   if (decoded) {
//     res.locals.user = decoded;
//     return next();
//   }
//   if (expired && refreshToken) {
//     const newAccessToken = await reIssueAccessToken({ refreshToken });
//     if (newAccessToken) {
//       res.header("x-access-token", newAccessToken);
//     }
//     const result = verifyJWT(newAccessToken);
//     res.locals.user = result.decoded;
//   }

//   return res.status(401).send("token is expired");
// };
